package com.company;

public enum Opcoes {
    BANANA(10),
    FRAMBOESA(50),
    MOEDA(100),
    SETE(300),
    ;

    private int value;

    Opcoes(int valor) {
        this.value = valor;
    }
    public int getValue() {
        return value;
    }
}
