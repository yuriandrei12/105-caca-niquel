package com.company;

import java.util.ArrayList;

public class Pontuacao {
    ArrayList<Opcoes> sorteados = new ArrayList<>();
    int pontuacao;
    boolean tresIguais;

    public int getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }

    public int calcularPontuacao(){
        CacaNiquel cacaNiquel = new CacaNiquel();
        for (Opcoes sorteado : sorteados){
            pontuacao += sorteado.getValue();
        }
        if ((sorteados.get(0).equals(sorteados.get(1))) && sorteados.get(1).equals(sorteados.get(2))){
            pontuacao = pontuacao * 10;
        }

        return pontuacao;
    }

}
